odinLite
    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/home");


        $stateProvider

        //------------------------------
        // HOME
        //------------------------------

            .state('home', {
            url: '/home',
            templateUrl: 'views/home.html',
            resolve: {
                loadPlugin: function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'css',
                        insertBefore: '#app-level',
                        files: [
                            'vendors/bower_components/fullcalendar/dist/fullcalendar.min.css',
                        ]
                    }, {
                        name: 'vendors',
                        insertBefore: '#app-level-js',
                        files: [
                            'vendors/sparklines/jquery.sparkline.min.js',
                            'vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js',
                            'vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js'
                        ]
                    }])
                }
            }
        })


        //------------------------------
        // HEADERS
        //------------------------------
        .state('headers', {
            url: '/headers',
            templateUrl: 'views/common-2.html'
        })

        .state('headers.textual-menu', {
            url: '/textual-menu',
            templateUrl: 'views/textual-menu.html'
        })

        .state('headers.image-logo', {
            url: '/image-logo',
            templateUrl: 'views/image-logo.html'
        })

        .state('headers.mainmenu-on-top', {
            url: '/mainmenu-on-top',
            templateUrl: 'views/mainmenu-on-top.html'
        })


        //------------------------------
        // TYPOGRAPHY
        //------------------------------

        .state('typography', {
            url: '/typography',
            templateUrl: 'views/typography.html'
        })




        //------------------------------
        // TABLES
        //------------------------------

        .state('tables', {
            url: '/tables',
            templateUrl: 'views/common.html'
        })

        .state('tables.tables', {
            url: '/tables',
            templateUrl: 'views/tables.html'
        })

        .state('tables.data-table', {
                url: '/data-table',
                templateUrl: 'views/data-table.html'
            })
            .state('pages.shifts-list', {
                url: '/shifts-list',
                templateUrl: 'views/shifts-list.html'
            })
            .state('pages.premises', {
                url: '/premises',
                templateUrl: 'views/premises.html'
            })
              .state('pages.new-incident', {
                url: '/new-incident',
                templateUrl: 'views/new-incident.html'
            })
            
        //------------------------------
        // FORMS
        //------------------------------
        .state('form', {
            url: '/form',
            templateUrl: 'views/common.html'
        })

        .state('form.basic-form-elements', {
            url: '/basic-form-elements',
            templateUrl: 'views/form-elements.html',
            resolve: {
                loadPlugin: function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'vendors',
                        files: [
                            'vendors/bower_components/autosize/dist/autosize.min.js'
                        ]
                    }])
                }
            }
        })

        .state('form.form-components', {
            url: '/form-components',
            templateUrl: 'views/form-components.html',
            resolve: {
                loadPlugin: function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'css',
                        insertBefore: '#app-level',
                        files: [
                            'vendors/bower_components/nouislider/jquery.nouislider.css',
                            'vendors/farbtastic/farbtastic.css',
                            'vendors/bower_components/summernote/dist/summernote.css',
                            'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                            'vendors/bower_components/chosen/chosen.min.css'
                        ]
                    }, {
                        name: 'vendors',
                        files: [
                            'vendors/input-mask/input-mask.min.js',
                            'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                            'vendors/bower_components/moment/min/moment.min.js',
                            'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                            'vendors/bower_components/summernote/dist/summernote.min.js',
                            'vendors/fileinput/fileinput.min.js',
                            'vendors/bower_components/chosen/chosen.jquery.js',
                            'vendors/bower_components/angular-chosen-localytics/chosen.js',
                            'vendors/bower_components/angular-farbtastic/angular-farbtastic.js'
                        ]
                    }])
                }
            }
        })




        //------------------------------
        // USER INTERFACE
        //------------------------------

        .state('user-interface', {
            url: '/user-interface',
            templateUrl: 'views/common.html'
        })

        .state('user-interface.ui-bootstrap', {
            url: '/ui-bootstrap',
            templateUrl: 'views/ui-bootstrap.html'
        })

        .state('user-interface.colors', {
            url: '/colors',
            templateUrl: 'views/colors.html'
        })

        .state('user-interface.animations', {
            url: '/animations',
            templateUrl: 'views/animations.html'
        })

        .state('user-interface.box-shadow', {
            url: '/box-shadow',
            templateUrl: 'views/box-shadow.html'
        })

        .state('user-interface.buttons', {
            url: '/buttons',
            templateUrl: 'views/buttons.html'
        })

        .state('user-interface.icons', {
            url: '/icons',
            templateUrl: 'views/icons.html'
        })

        .state('user-interface.alerts', {
            url: '/alerts',
            templateUrl: 'views/alerts.html'
        })

        .state('user-interface.preloaders', {
            url: '/preloaders',
            templateUrl: 'views/preloaders.html'
        })

        .state('user-interface.notifications-dialogs', {
            url: '/notifications-dialogs',
            templateUrl: 'views/notification-dialog.html'
        })

        .state('user-interface.media', {
            url: '/media',
            templateUrl: 'views/media.html',
            resolve: {
                loadPlugin: function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'css',
                        insertBefore: '#app-level',
                        files: [
                            'vendors/bower_components/mediaelement/build/mediaelementplayer.css',
                            'vendors/bower_components/lightgallery/light-gallery/css/lightGallery.css'
                        ]
                    }, {
                        name: 'vendors',
                        files: [
                            'vendors/bower_components/mediaelement/build/mediaelement-and-player.js',
                            'vendors/bower_components/lightgallery/light-gallery/js/lightGallery.min.js'
                        ]
                    }])
                }
            }
        })



        //------------------------------
        // GENERIC CLASSES
        //------------------------------

        .state('generic-classes', {
            url: '/generic-classes',
            templateUrl: 'views/generic-classes.html'
        })


        //------------------------------
        // PAGES
        //------------------------------

        .state('pages', {
            url: '/pages',
            templateUrl: 'views/common.html'
        })


        //Profile

        .state('pages.profile', {
            url: '/profile',
            templateUrl: 'views/profile.html'
        })

        .state('pages.profile.profile-about', {
            url: '/profile-about',
            templateUrl: 'views/profile-about.html'
        })



        .state('pages.pricing-table', {
            url: '/pricing-table',
            templateUrl: 'views/pricing-table.html'
        })

 

        .state('pages.invoice', {
            url: '/invoice',
            templateUrl: 'views/invoice.html'
        })



    });
