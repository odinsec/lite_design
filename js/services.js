odinLite

// =========================================================================
// Header Messages and Notifications list Data
// =========================================================================

    .service('messageService', ['$resource', function($resource) {
    this.getMessage = function(img, user, text) {
        var gmList = $resource("data/messages-notifications.json");

        return gmList.get({
            img: img,
            user: user,
            text: text
        });
    }
}])


// =========================================================================
// Best Selling Widget Data (Home Page)
// =========================================================================

.service('bestsellingService', ['$resource', function($resource) {
    this.getBestselling = function(img, name, range) {
        var gbList = $resource("data/best-selling.json");

        return gbList.get({
            img: img,
            name: name,
            range: range,
        });
    }
}])


// =========================================================================
// Todo List Widget Data
// =========================================================================

.service('todoService', ['$resource', function($resource) {
    this.getTodo = function(todo) {
        var todoList = $resource("data/todo.json");

        return todoList.get({
            todo: todo
        });
    }
}])


// =========================================================================
// Recent Items Widget Data
// =========================================================================

.service('recentitemService', ['$resource', function($resource) {
    this.getRecentitem = function(id, name, price) {
        var recentitemList = $resource("data/recent-items.json");

        return recentitemList.get({
            id: id,
            name: name,
            price: price
        })
    }
}])


// =========================================================================
// Recent Posts Widget Data
// =========================================================================

.service('recentpostService', ['$resource', function($resource) {
    this.getRecentpost = function(img, user, text) {
        var recentpostList = $resource("data/messages-notifications.json");

        return recentpostList.get({
            img: img,
            user: user,
            text: text
        })
    }
}])

// =========================================================================
// Data Table
// =========================================================================

.service('tableService', [function() {
    this.users = [{
        "id": 10238,
        "name": "Test User",
        "email": "test@example.com",
        "username": "test",
        "contact": "122-5003"
    }, {
        "id": 10238,
        "name": "Test User",
        "email": "test@example.com",
        "username": "test",
        "contact": "122-5003"
    }];


    this.data = [{
            "id": "56838",
            "name": "Joe Bloggs",
            "description": "recurring shift",
            "start": "02/08/2016 17:00",
            "end": "02/08/2016 23:00",
            "break": "21:00",
            "notes": "",
            "employees": "3"
        }, {
            "id": "49309",
            "name": "Fred Test",
            "description": "Single Shift",
            "start": "31/07/2016 10:00",
            "end": "31/07/2016 19:00",
            "break": "15:00",
            "notes": "",
            "employees": "3"
        }

    ];

}])


// =========================================================================
// Malihu Scroll - Custom Scroll bars
// =========================================================================
.service('scrollService', function() {
    var ss = {};
    ss.malihuScroll = function scrollBar(selector, theme, mousewheelaxis) {
        $(selector).mCustomScrollbar({
            theme: theme,
            scrollInertia: 100,
            axis: 'yx',
            mouseWheel: {
                enable: true,
                axis: mousewheelaxis,
                preventDefault: true
            }
        });
    }

    return ss;
})


//==============================================
// BOOTSTRAP GROWL
//==============================================

.service('growlService', function() {
    var gs = {};
    gs.growl = function(message, type) {
        $.growl({
            message: message
        }, {
            type: type,
            allow_dismiss: false,
            label: 'Cancel',
            className: 'btn-xs btn-inverse',
            placement: {
                from: 'top',
                align: 'right'
            },
            delay: 2500,
            animate: {
                enter: 'animated bounceIn',
                exit: 'animated bounceOut'
            },
            offset: {
                x: 20,
                y: 85
            }
        });
    }

    return gs;
})
